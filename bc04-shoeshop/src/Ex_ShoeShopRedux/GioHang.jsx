import React, { Component } from "react";
import { connect } from "react-redux";
import {
  DECREASE_QUANTITY,
  INCREASE_QUANTITY,
  REMOVE_ITEM,
} from "./redux/constants/shoeConstants";

class GioHang extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            {" "}
            <img src={item.image} style={{ width: 80 }} alt="" />
          </td>
          <td>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.decreaseQty(item);
              }}
            >
              -
            </button>
            <span className="mx-3">{item.soLuong}</span>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.increaseQty(item);
              }}
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.removeItem(item);
              }}
              className="btn btn-danger"
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div className="container py-5">
        {/* gioHang */}
        <table className="table text-left">
          <thead>
            <tr>
              <th>Tên</th>
              <th>Giá</th>
              <th>Hình ảnh</th>
              <th>Số lượng</th>
            </tr>
          </thead>

          <tbody>{this.renderTbody()}</tbody>
        </table>

        {this.props.gioHang.length == 0 && (
          <p className=" mt-5 text-center">Chưa có sản phẩm trong giỏi hàng</p>
        )}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.gioHang,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    decreaseQty: (value) => {
      dispatch({
        type: DECREASE_QUANTITY,
        payload: value,
      });
    },
    increaseQty: (value) => {
      dispatch({
        type: INCREASE_QUANTITY,
        payload: value,
      });
    },
    removeItem: (value) => {
      dispatch({
        type: REMOVE_ITEM,
        payload: value,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(GioHang);
