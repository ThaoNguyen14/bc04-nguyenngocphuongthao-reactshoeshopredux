import { dataShoe } from "../../data_shoe";
import {
  XEM_CHI_TIET,
  ADD_TO_CART,
  INCREASE_QUANTITY,
  DECREASE_QUANTITY,
  REMOVE_ITEM,
} from "../constants/shoeConstants";

let initialState = {
  shoeArr: dataShoe,
  detailShoe: dataShoe[0],
  gioHang: [],
};
// rxreducer
export let shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case XEM_CHI_TIET: {
      state.detailShoe = action.payload;
      return { ...state };
    }
    case ADD_TO_CART: {
      let shoe = action.payload;
      let cloneGioHang = [...state.gioHang]; // Deep clone lv1

      let index = state.gioHang.findIndex((item) => {
        return item.id == shoe.id;
      });

      // th1 sản phẩm chưa có trong giỏi hàng
      if (index == -1) {
        let spGioHang = { ...shoe, soLuong: 1 };
        console.log("spGioHang: ", spGioHang);
        cloneGioHang.push(spGioHang);
      } else {
        // th2 sản phẩm đã có trong giỏi hàng
        // let sp = cloneGioHang[index];
        // // tăng số lượng sp lên 1 đơn vị
        // sp.soLuong++;
        // cloneGioHang[index] = sp;
        cloneGioHang[index].soLuong++;
      }
      state.gioHang = cloneGioHang;
      return { ...state, gioHang: cloneGioHang };
    }
    case INCREASE_QUANTITY: {
      let shoe = action.payload;
      let cloneGioHang = [...state.gioHang]; // Deep clone lv1

      let index = state.gioHang.findIndex((item) => {
        return item.id == shoe.id;
      });
      if (index > -1) {
        cloneGioHang[index].soLuong++;
      }
      return { ...state, gioHang: cloneGioHang };
    }
    case DECREASE_QUANTITY: {
      let shoe = action.payload;
      let cloneGioHang = [...state.gioHang]; // Deep clone lv1

      let index = state.gioHang.findIndex((item) => {
        return item.id == shoe.id;
      });
      if (index > -1) {
        cloneGioHang[index].soLuong--;
      }
      return { ...state, gioHang: cloneGioHang };
    }
    case REMOVE_ITEM: {
      let shoe = action.payload;
      let cloneGioHang = [...state.gioHang]; // Deep clone lv1

      let index = state.gioHang.findIndex((item) => {
        return item.id == shoe.id;
      });
      if (index > -1) {
        cloneGioHang.splice(index, 1);
      }
      return { ...state, gioHang: cloneGioHang };
    }
    default:
      return state;
  }
};
